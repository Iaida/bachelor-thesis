#pragma once
//#include "clouds.h"

#include "cellAttr.h"
#include "config.h"

// Volume with 3 booleans for each cell
std::vector<cellAttr> booleanVolume(exRes* exRes* exRes);
// variables to save old step boolean for simulation
std::vector<cellAttr> oldBooleanVolume;

void placeSeeds()
{
	std::cout << "placing act seeds in extended boolean window...\n";

	const int rangeAroundMid = exRes / 8;

	for (int s = 0; s < numSeeds; s++)
	{
		int x = continuousToDiscrete((exRes / 2) + (randomFloat() * rangeAroundMid));
		int y = continuousToDiscrete((exRes / 2) + randomFloat());
		int z = continuousToDiscrete((exRes / 2) + (randomFloat() * rangeAroundMid));

		int currentIndex = exVolumeIndexFromCoordinates(x, y, z);
		booleanVolume[currentIndex].hum = false;
		booleanVolume[currentIndex].act = true;
	}


	/*float radius = (float)exRes / 8.0f;
	vmath::vec3 center = { (float)exRes / 2.0f, (float)exRes / 2.0f, (float)exRes / 2.0f };

	for (int s = 0; s < numSeeds; s++)
	{
		float phi = 2.0f*(float)M_PI*(float)rand() / (float)RAND_MAX;
		float length = radius*(float)rand() / (float)RAND_MAX;
		vmath::vec3 offset = { cos(phi)*length, randomFloat(), sin(phi)*length };

		vmath::vec3 seedLoc = center + offset;
		int currentIndex = exVolumeIndexFromCoordinates((int)seedLoc[0], (int)seedLoc[1], (int)seedLoc[2]);
		booleanVolume[currentIndex].hum = false;
		booleanVolume[currentIndex].act = true;
	}*/
}



void initializeBooleanCells()
{
	std::cout << "initializing boolean cells inside data window...\n";

	for (int z = padding; z < exBound; z++)
	{
		for (int y = padding; y < exBound; y++)
		{
			for (int x = padding; x < exBound; x++)
			{
				int currentIndex = exVolumeIndexFromCoordinates(x, y, z);
				booleanVolume[currentIndex].hum = randomBoolean();
				booleanVolume[currentIndex].act = false; //changed in next step
				booleanVolume[currentIndex].cld = false;
				booleanVolume[currentIndex].ext = false;
				booleanVolume[currentIndex].extCountdown = extTime;
			}
		}
	}
}




bool actFunc(const unsigned i, const unsigned j, const unsigned k)
{
	// boundary cases avoided with padding >=2
	return oldBooleanVolume[exVolumeIndexFromCoordinates(i + 1, j, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j + 1, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k + 1)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i - 1, j, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j - 1, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k - 1)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i - 2, j, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i + 2, j, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j - 2, k)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k + 2)].act ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k - 2)].act;

	//// simplified version

	//return oldBooleanVolume[exVolumeIndexFromCoordinates(i + 1, j, k)].act ||
	//	oldBooleanVolume[exVolumeIndexFromCoordinates(i - 1, j, k)].act ||
	//	oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k - 1)].act ||
	//	oldBooleanVolume[exVolumeIndexFromCoordinates(i, j - 1, k)].act;

}

bool extFunc(const unsigned i, const unsigned j, const unsigned k)
{
	return oldBooleanVolume[exVolumeIndexFromCoordinates(i + 1, j, k)].ext ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i - 1, j, k)].ext ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j + 1, k)].ext ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j - 1, k)].ext ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k + 1)].ext ||
		oldBooleanVolume[exVolumeIndexFromCoordinates(i, j, k - 1)].ext;
}



float getDensityFromAveragedCloudExistance(const unsigned i, const unsigned j, const unsigned k)
{

	



	// weighted average of 3x3x3 cube
	return selfWeight * booleanVolume[exVolumeIndexFromCoordinates(i, j, k)].cld +
		// 6 cells that share a face
		axisWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j, k)].cld +
		axisWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j, k)].cld +
		axisWeight * booleanVolume[exVolumeIndexFromCoordinates(i, j + 1, k)].cld +
		axisWeight * booleanVolume[exVolumeIndexFromCoordinates(i, j - 1, k)].cld +
		axisWeight * booleanVolume[exVolumeIndexFromCoordinates(i, j, k + 1)].cld +
		axisWeight * booleanVolume[exVolumeIndexFromCoordinates(i, j, k - 1)].cld +
		// 8 corners of 3x3x3 cube
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j + 1, k + 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j - 1, k - 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j - 1, k - 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j + 1, k + 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j + 1, k - 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j - 1, k + 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j + 1, k - 1)].cld +
		cornerWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j - 1, k + 1)].cld +
		// 12 diagonal neighbors
		// starting with top 3x3 slice
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j + 1, k - 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j + 1, k + 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j + 1, k - 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j + 1, k + 1)].cld +
		// middle 3x3 slice
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j, k - 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j, k + 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j, k - 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j, k + 1)].cld +
		// bottom 3x3 slice
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j - 1, k - 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j - 1, k + 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i + 1, j - 1, k - 1)].cld +
		diagWeight * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j - 1, k + 1)].cld;


	//// low pass filter
	//float result = 0.0f;
	//for (int z = -1; z < 2; z++)
	//{
	//	for (int y = -1; y < 2; y++)
	//	{
	//		for (int z = -1; z < 2; z++)
	//		{
	//			result += (1.0f / 27.0f) * booleanVolume[exVolumeIndexFromCoordinates(i - 1, j - 1, k - 1)].cld;
	//			if (result > 0.0f)
	//				int bpoint = 0;
	//		}
	//	}
	//}
	//return result;


	//// Gaussian

	//// x axis
	//// get mean
	//float meanX = 0.0f;
	//for (int x = -1; x < 2; x++)
	//{
	//	if (booleanVolume[exVolumeIndexFromCoordinates(i + x, j, k)].cld)
	//	{
	//		meanX += 1.0f;
	//	}
	//}
	//meanX /= 3.0f;

	//// get standard deviation
	//float sigmaX = 0.0f;
	//for (int x = -1; x < 2; x++)
	//{
	//	if (booleanVolume[exVolumeIndexFromCoordinates(i + x, j, k)].cld)
	//	{
	//		sigmaX += (1.0f - meanX)*(1.0f - meanX);
	//	}
	//	else
	//	{
	//		sigmaX += (0.0f - meanX)*(0.0f - meanX);
	//	}
	//}
	//sigmaX = sqrt(sigmaX / 3.0f);

	//// apply Gaussian
	//float resultX = 0.0f;
	//for (int x = -1; x < 2; x++)
	//{
	//	float gaussian = (1.0f / sqrt(2.0f * (float)M_PI * sigmaX * sigmaX)) * 
	//		exp(-(float)(x*x) / (2 * sigmaX * sigmaX));
	//	if (booleanVolume[exVolumeIndexFromCoordinates(i + x, j, k)].cld)
	//	{
	//		resultX += gaussian * 1.0f;
	//	}
	//}


	//// y axis
	//// get mean
	//float meanY = 0.0f;
	//for (int y = -1; y < 2; y++)
	//{
	//	if (y == 0)
	//	{
	//		meanY += resultX;
	//	}

	//	else if (booleanVolume[exVolumeIndexFromCoordinates(i, j + y, k)].cld)
	//	{
	//		meanY += 1.0f;
	//	}
	//}
	//meanY /= 3.0f;

	//// get standard deviation
	//float sigmaY = 0.0f;
	//for (int y = -1; y < 2; y++)
	//{
	//	if (y == 0)
	//	{
	//		sigmaY += (resultX - meanY)*(resultX - meanY);
	//	}

	//	else if (booleanVolume[exVolumeIndexFromCoordinates(i, j + y, k)].cld)
	//	{
	//		sigmaY += (1.0f - meanY)*(1.0f - meanY);
	//	}
	//	else
	//	{
	//		sigmaY += (0.0f - meanY)*(0.0f - meanY);
	//	}
	//}
	//sigmaY = sqrt(sigmaY / 3.0f);

	//// apply Gaussian
	//float resultY = 0.0f;
	//for (int y = -1; y < 2; y++)
	//{
	//	float gaussian = (1.0f / sqrt(2.0f * (float)M_PI * sigmaY * sigmaY)) *
	//		exp(-(float)(y*y) / (2 * sigmaY * sigmaY));

	//	if (y == 0)
	//	{
	//		resultY += gaussian * resultX;
	//	}

	//	else if (booleanVolume[exVolumeIndexFromCoordinates(i, j + y, k)].cld)
	//	{
	//		resultY += gaussian * 1.0f;
	//	}
	//}

	//// z axis
	//// get mean
	//float meanZ = 0.0f;
	//for (int z = -1; z < 2; z++)
	//{
	//	if (z == 0)
	//	{
	//		meanZ += resultY;
	//	}

	//	else if (booleanVolume[exVolumeIndexFromCoordinates(i, j, k + z)].cld)
	//	{
	//		meanZ += 1.0f;
	//	}
	//}
	//meanZ /= 3.0f;

	//// get standard deviation
	//float sigmaZ = 0.0f;
	//for (int z = -1; z < 2; z++)
	//{
	//	if (z == 0)
	//	{
	//		sigmaZ += (resultY - meanZ)*(resultY - meanZ);
	//	}

	//	else if (booleanVolume[exVolumeIndexFromCoordinates(i, j, k + z)].cld)
	//	{
	//		sigmaZ += (1.0f - meanZ)*(1.0f - meanZ);
	//	}
	//	else
	//	{
	//		sigmaZ += (0.0f - meanZ)*(0.0f - meanZ);
	//	}
	//}
	//sigmaZ = sqrt(sigmaZ / 3.0f);

	//// apply Gaussian
	//float resultZ = 0.0f;
	//for (int z = -1; z < 2; z++)
	//{
	//	float gaussian = (1.0f / sqrt(2.0f * (float)M_PI * sigmaZ * sigmaZ)) *
	//		exp(-(float)(z*z) / (2 * sigmaZ * sigmaZ));

	//	if (z == 0)
	//	{
	//		resultZ += gaussian * resultY;
	//	}

	//	else if (booleanVolume[exVolumeIndexFromCoordinates(i, j, k + z)].cld)
	//	{
	//		resultZ += gaussian * 1.0f;
	//	}
	//}

	//return resultZ;
}



void simulateCellularAutomaton()
{


	// start simulation with cellular automaton

	bool tempAct, tempHum, tempCld, tempExt;


	for (int i = 0; i < simSteps; i++)
	{
		int percentDone = (int)(100 * (float)i / (float)simSteps);
		std::cout << percentDone << "% done. Calculating simulation step number " << i + 1 << " of " << simSteps << "...\n";

		// save old step boolean Volume
		oldBooleanVolume = booleanVolume;

		int x, y, z, currentIndex;
#pragma omp parallel for private(y,x,currentIndex,tempAct, tempHum, tempCld, tempExt)
		for (z = padding; z < exBound; z++)
		{
			for (y = padding; y < exBound; y++)
			{
				for (x = padding; x < exBound; x++)
				{
					currentIndex = exVolumeIndexFromCoordinates(x, y, z);

					// save old step boolean values
					tempAct = oldBooleanVolume[currentIndex].act;
					tempHum = oldBooleanVolume[currentIndex].hum;
					tempCld = oldBooleanVolume[currentIndex].cld;
					tempExt = oldBooleanVolume[currentIndex].ext;

					// calculate new boolean with Nagel's Growth Simulation
					booleanVolume[currentIndex].act = (!tempAct) && tempHum && actFunc(x, y, z);
					booleanVolume[currentIndex].hum = tempHum && (!tempAct);

					// with extinction
					booleanVolume[currentIndex].ext = (!tempExt) && tempCld && extFunc(x, y, z);

					// with unnatural transition 
					booleanVolume[currentIndex].cld = (!tempExt) && (tempCld || tempAct);

				}
			}
		}
	}
}



void boolToFloatConversion()
{

	std::cout << "calculating floats from booleans in data window part of the extended float array...\n";

	for (int z = padding; z < exBound; z++)
	{
		for (int y = padding; y < exBound; y++)
		{
			for (int x = padding; x < exBound; x++)
			{
				int currentIndex = exVolumeIndexFromCoordinates(x, y, z);
				// calculate float value from bool cells
				exDensityVolume[currentIndex] = getDensityFromAveragedCloudExistance(x, y, z);
			}
		}
	}
}



void executeCellularAutomaton()
{
	initializeBooleanCells();
	placeSeeds();
	simulateCellularAutomaton();
	boolToFloatConversion();
	removePadding();
	writeVolumeToDisk(dWDensityVolume);
}
