#pragma once
#include "utility.h"
#include "config.h"
#include "clouds.h"




float getAbsoluteDifferenceSH(const std::vector<float>& coeffsA, const std::vector<float>& coeffsB)
{
	size_t numCoeffs = coeffsA.size();
	float acc = 0.0f;

	for (size_t i = 0; i < numCoeffs; i++)
	{
		acc += abs(coeffsA[i] - coeffsB[i]);
	}
	return acc;
}


float getSHSimilarityAtVoxel(const unsigned x, const unsigned y, const unsigned z)
{
	if (x == 0 || x == dWRes - 1 || y == 0 || y == dWRes - 1 || z == 0 || z == dWRes - 1)
	{
		// border case
		return 0.0f;
	}

	float acc = 0.0f;

	// absolute difference accumulated over 3x3 cube
	std::vector<float> centralCoeffs = volumeSH[dWVolumeIndexFromCoordinates(x, y, z)];

	// x+1 slice
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y, z - 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y + 1, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y + 1, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y + 1, z - 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y - 1, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y - 1, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x + 1, y - 1, z - 1)]);

	// x-1 slice
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y, z - 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y + 1, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y + 1, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y + 1, z - 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y - 1, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y - 1, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x - 1, y - 1, z - 1)]);

	// x slice
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y, z - 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y + 1, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y + 1, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y + 1, z - 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y - 1, z)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y - 1, z + 1)]);
	acc += getAbsoluteDifferenceSH(centralCoeffs, volumeSH[dWVolumeIndexFromCoordinates(x, y - 1, z - 1)]);

	return acc;
}


void calculateSHSimilarity()
{
	std::cout << "Calculating Volume SH similarities...\n";

	int counter = 1;
	int percentDone = 0;

	int x, y, z, newPercentage, currentIndex;
#pragma omp parallel for private(y,x,newPercentage, currentIndex)
	for (z = 0; z < dWRes; z++)
	{
		for (y = 0; y < dWRes; y++)
		{
			for (x = 0; x < dWRes; x++)
			{
				newPercentage = (int)(100 * (float)counter / (float)(dWRes * dWRes * dWRes));
				if (newPercentage > percentDone)
				{
					percentDone = newPercentage;
					std::cout << percentDone << "% done. Calculating voxel number " << counter << " of " << dWRes * dWRes * dWRes << "...\n";
				}

				currentIndex = dWVolumeIndexFromCoordinates(x, y, z);


				/*if (dWDensityVolume[currentIndex] > 0.0f)
				{
					volumeSHSimilarity[currentIndex] = getSHSimilarityAtVoxel(x, y, z);
				}*/

				volumeSHSimilarity[currentIndex] = getSHSimilarityAtVoxel(x, y, z);


				counter++;
			}
		}
	}
}

