#pragma once

#include "config.h"
#include "Color.h"


#include <functional>
#include <optional>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>




// conversion from float values to int indices
int continuousToDiscrete(float contCoord)
{
	return static_cast<int>(std::floor(contCoord));
}

// conversion from int indices to float values
float discreteToContinuous(int discCoord)
{
	return static_cast<float>(discCoord) + 0.5f;
}



unsigned Index1DFrom3D(const unsigned i, const unsigned j, const unsigned k, const unsigned resolution)
{
	return i + j * resolution + k * resolution * resolution;
}


bool randomBoolean()
{
	return rand() % 2 == 0;
}


// returns float from -1 to 1
float randomFloat()
{
	return 2.0f * (((float)rand() / (float)RAND_MAX) - 0.5f);
}


float lerp(const float a, const float b, const float t)
{
	return (1.0f - t) * a + t * b;
}


// http://en.wikipedia.org/wiki/Trilinear_interpolation
// Interpolation for arbitrary point in the volume

// TODO: performance
float trilinearInterpolation(const std::vector<float>& vec, float x, float y, float z, const std::function<unsigned(unsigned, unsigned, unsigned)> & indexMapping)
{
	const int x0 = (int)floor(x);
	const int x1 = (int)ceil(x);
	// x1 - x0 results in division by 0, if x whole-number
	//float xd = (x - x0) / (x1 - x0); 
	float xd = (x - x0);

	const int y0 = (int)floor(y);
	const int y1 = (int)ceil(y);
	//float yd = (y - y0) / (y1 - y0);
	float yd = (y - y0);

	const int z0 = (int)floor(z);
	const int z1 = (int)ceil(z);
	//float zd = (z - z0) / (z1 - z0);
	float zd = (z - z0);

	const float c000 = vec[indexMapping(x0, y0, z0)];
	const float c100 = vec[indexMapping(x1, y0, z0)];
	const float c001 = vec[indexMapping(x0, y0, z1)];
	const float c101 = vec[indexMapping(x1, y0, z1)];
	const float c010 = vec[indexMapping(x0, y1, z0)];
	const float c110 = vec[indexMapping(x1, y1, z0)];
	const float c011 = vec[indexMapping(x0, y1, z1)];
	const float c111 = vec[indexMapping(x1, y1, z1)];

	const float c00 = lerp(c000, c100, xd);
	const float c01 = lerp(c001, c101, xd);
	const float c10 = lerp(c010, c110, xd);
	const float c11 = lerp(c011, c111, xd);

	const float c0 = lerp(c00, c10, yd);
	const float c1 = lerp(c01, c11, yd);

	return lerp(c0, c1, zd);
}



// http://www.astro.umd.edu/~jph/HG_note.pdf (7)
// returns values from 0 to pi
float angleHG()
{
	// TODO
	// between 0 and 1
	float rng = (float)rand() / (float)RAND_MAX;
	if (rng == 0.0f)
	{
		// would result in uncomputable calculation
		rng = FLT_EPSILON;
	}
	float result = acos((1.0f / (2.0f * g)) * (1.0f + g * g - powf((1.0f - g * g) / (1.0f + g * (2 * rng - 1)), 2.0f)));
	return result;
}



// http://www.scratchapixel.com/old/lessons/3d-advanced-lessons/volume-rendering/volume-rendering-for-artists/
float getTransmittance(float density)
{
	// e^(-density*(absorption coeff + scattering coeff)*pathlength
	// no absorption, only scattering, pathlength=1

	// simple version, density = 0 -> returns 1
	return exp(-density) * samplingDistance;
}


// range [0,1), including 0, excluding 1
float canonicalRandomVariable()
{
	return (float)rand() / ((float)RAND_MAX + 1.0f);
}


float clip(float n, float lower, float upper)
{
	return std::max(lower, std::min(n, upper));
}


bool compareTheta(const vmath::vec3 & u, const vmath::vec3 & v)
{
	float thetaU = atan(u[1] / u[0]);
	float thetaV = atan(v[1] / v[0]);
	return (thetaU < thetaV);
}


bool comparePhi(const vmath::vec3 & u, const vmath::vec3 & v)
{
	float phiU = acos(u[2] / vmath::length(u));
	float phiV = acos(v[2] / vmath::length(v));
	return (phiU < phiV);
}

float gaussianFunction1D(float distance, float sigma)
{
	return exp(-(distance * distance) / (2 * sigma * sigma)) / (sqrt(2.0f * (float)M_PI * sigma * sigma));
}




// reduces ringing artifacts / Gibbs phenomenon
float getHeatKernel(int coeff)
{
	// fitting for order of 6 -> results from 1 to 0.1
	float sigma = 0.05f;

	// get l from c/coeffindex
	// 0 -> 0, 1-3 -> 1, 4-8 -> 2, 9-15 -> 3
	int l = (int)sqrt(coeff);

	return exp(-(float)l * ((float)l + 1.0f) * sigma);
}


float getNormalizationWeights(int coeff)
{
	int l = (int)sqrt(coeff);
	return sqrt((4.0f * (float)M_PI) / (2.0f * l + 1.0f));
}



float lengthSH(const std::vector<float> & SH)
{
	float length = 0.0f;
	for (size_t n = 0; n < SH.size(); n++)
	{
		length += SH[n] * SH[n];
	}
	return sqrt(length);
}




// tavianator fast branchless raybounding box intersections

std::optional<vmath::vec2> rayBoxIntersection(const ray  & r, const Box & b)
{

	// x-axis

	float tx1 = (b.min[0] - r.origin[0]) / r.direction[0];
	float tx2 = (b.max[0] - r.origin[0]) / r.direction[0];

	float tmin = std::min(tx1, tx2);
	float tmax = std::max(tx1, tx2);


	// y-axis

	float ty1 = (b.min[1] - r.origin[1]) / r.direction[1];
	float ty2 = (b.max[1] - r.origin[1]) / r.direction[1];

	tmin = std::max(tmin, std::min(ty1, ty2));
	tmax = std::min(tmax, std::max(ty1, ty2));


	// z-axis

	float tz1 = (b.min[2] - r.origin[2]) / r.direction[2];
	float tz2 = (b.max[2] - r.origin[2]) / r.direction[2];

	tmin = std::max(tmin, std::min(tz1, tz2));
	tmax = std::min(tmax, std::max(tz1, tz2));

	if (tmax < tmin)
		return std::nullopt;

	return vmath::vec2{ tmin, tmax };
}


// http://www.astro.umd.edu/~jph/HG_note.pdf (6)
// returns values between 0 and 1
float weightHG(float angle)
{
	return ((1.0f - g * g) / (2.0f * g)) * 
		(pow(1.0f + g * g - 2.0f * g * cos(angle), -0.5f) - (1.0f / (1.0f + g)));
	
}


// http://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
// error of 10^-3 magnitude
vmath::vec3 rotateVectorAroundAxis(const vmath::vec3 & v, float radians, const vmath::vec3 & axis)
{
	return v * cos(radians) + vmath::cross(axis, v) * sin(radians) + axis * vmath::dot(axis, v) * (1 - cos(radians));
}

std::vector<float> normalizeSH(const std::vector<float>& SH)
{
	std::vector<float> result(SH.size());
	// get length
	float length = lengthSH(SH);
	// scale
	for (size_t n = 0; n < SH.size(); n++)
	{
		result[n] = SH[n] / length;
	}
	return result;
}