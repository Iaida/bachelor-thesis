#pragma once

#include "config.h"
#include "utility.h"
#include "clouds.h"

#include <iostream>
#include <fstream>
#include <algorithm>

//// file management
const char* fileNameVolume = "3D Volume.";
const char* fileNameVolumeSH = "3D Volume SH.";
const char* fileNameRadiance = "3D Volume Radiance Estimate.";
const char* fileNameExtinction = "3D Volume Extinction.";
const char* fileNameVolumeSHSimilarity = "3D Volume SH Similarity.";




template<typename Container>
void writeContainerAsBinary(const Container& container, const char* fileName)
{
	std::FILE* file;

	if (fopen_s(&file, fileName, "wb") != 0)
	{
		std::cout << "opening file " << fileName << "failed \n";
		return;
	}

	// write buffer to file: https://en.cppreference.com/w/cpp/io/c/fwrite
	std::fwrite(container.data(), sizeof(container[0]), container.size(), file);
	std::fclose(file);
}



void writeVolumeSHSimilarityToDisk(const std::vector<float>& volumeSHSimilarity)
{
	std::cout << "writing SH similarity array content to disc...\n";
	writeContainerAsBinary(volumeSHSimilarity, fileNameVolumeSHSimilarity);
}



// Source: Rasterrain open source raytracer
void savebmp(const char* filename, int w, int h, int dpi, const std::vector<ColorF>& pixelsF) 
{
	// convert from float to uchar
	std::vector<ColorUChar> pixelsUChar;
	std::transform(pixelsF.begin(), pixelsF.end(), std::back_inserter(pixelsUChar), ColorFloatToUChar);


	std::cout << "saving as bmp...\n";

	int k = w * h;
	int s = 4 * k;
	int filesize = 54 + s;

	double factor = 39.375;
	int m = static_cast<int>(factor);

	int ppm = dpi * m;

	unsigned char bmpfileheader[14] = { 'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0 };
	unsigned char bmpinfoheader[40] = { 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0 };

	bmpfileheader[2] = (unsigned char)(filesize);
	bmpfileheader[3] = (unsigned char)(filesize >> 8);
	bmpfileheader[4] = (unsigned char)(filesize >> 16);
	bmpfileheader[5] = (unsigned char)(filesize >> 24);

	bmpinfoheader[4] = (unsigned char)(w);
	bmpinfoheader[5] = (unsigned char)(w >> 8);
	bmpinfoheader[6] = (unsigned char)(w >> 16);
	bmpinfoheader[7] = (unsigned char)(w >> 24);

	bmpinfoheader[8] = (unsigned char)(h);
	bmpinfoheader[9] = (unsigned char)(h >> 8);
	bmpinfoheader[10] = (unsigned char)(h >> 16);
	bmpinfoheader[11] = (unsigned char)(h >> 24);

	bmpinfoheader[21] = (unsigned char)(s);
	bmpinfoheader[22] = (unsigned char)(s >> 8);
	bmpinfoheader[23] = (unsigned char)(s >> 16);
	bmpinfoheader[24] = (unsigned char)(s >> 24);

	bmpinfoheader[25] = (unsigned char)(ppm);
	bmpinfoheader[26] = (unsigned char)(ppm >> 8);
	bmpinfoheader[27] = (unsigned char)(ppm >> 16);
	bmpinfoheader[28] = (unsigned char)(ppm >> 24);

	bmpinfoheader[29] = (unsigned char)(ppm);
	bmpinfoheader[30] = (unsigned char)(ppm >> 8);
	bmpinfoheader[31] = (unsigned char)(ppm >> 16);
	bmpinfoheader[32] = (unsigned char)(ppm >> 24);

	FILE* file;
	errno_t err;

	// open file for write
	err = fopen_s(&file, filename, "wb");
	if (err == 0)
	{
		std::cout << "open to write successfull\n";
	}
	else
	{
		std::cout << "open to write failed\n";
	}


	fwrite(bmpfileheader, 1, 14, file);
	fwrite(bmpinfoheader, 1, 40, file);


	std::fwrite(pixelsUChar.data(), sizeof(ColorUChar), pixelsUChar.size(), file);

	fclose(file);
}



void writeVolumeToDisk(const std::vector<float>& dWDensityVolume)
{
	std::cout << "writing data window float array content to disc...\n";
	writeContainerAsBinary(dWDensityVolume, fileNameVolume);
}



void loadVolumeFromDisk(std::vector<float>& dWDensityVolume, const char* fileName = fileNameVolume)
{
	FILE* file;
	errno_t err;

	//open to read data from file to array
	err = fopen_s(&file, fileName, "rb");
	if (err == 0)
	{
		std::cout << "open to read successful\n";
	}
	else
	{
		std::cout << "open to read failed\n";
		return;
	}

	std::cout << "reading data window float array content from disc...\n";

	for (int i = 0; i < dWRes * dWRes * dWRes; i++)
	{
		float v;
		fread(&v, sizeof(float), 1, file);
		dWDensityVolume[i] = v;
	}
	fclose(file);
}




void writeVolumeExtinctionToDisk(const std::vector<float>& exExtinctionVolume)
{
	std::cout << "writing extinction array content to disc...\n";
	writeContainerAsBinary(exExtinctionVolume, fileNameExtinction);
}


void loadVolumeExtinctionFromDisk(std::vector<float>& exExtinctionVolume, const char* fileName = fileNameExtinction)
{
	FILE* file;
	errno_t err;

	//open to read data from file to array

	err = fopen_s(&file, fileName, "rb");
	if (err == 0)
	{
		std::cout << "open to read successful\n";
	}
	else
	{
		std::cout << "open to read failed\n";
		return;
	}

	std::cout << "reading extended Volume Extinction from disc...\n";

	for (int i = 0; i < exRes * exRes * exRes; i++)
	{
		float v;
		fread(&v, sizeof(float), 1, file);
		exExtinctionVolume[i] = v;
	}
	fclose(file);
}


void writeScatterEventsToDisk(const std::vector<float>& scatterEventVertices, const std::vector<int>& scatterEventEdges)
{
	std::cout << "writing scatter events to disk...\n";

	std::ofstream stream("Scatter Events.ply");

	// header
	stream << "ply\n";
	stream << "format ascii 1.0\n";
	stream << "element vertex " << (scatterEventVertices.size() / 3) + 2 << "\n";
	stream << "property float x\n";
	stream << "property float y\n";
	stream << "property float z\n";
	stream << "element edge " << scatterEventEdges.size() / 2 << "\n";
	stream << "property int vertex1\n";
	stream << "property int vertex2\n";
	stream << "property uchar red\n";
	stream << "property uchar green\n";
	stream << "property uchar blue\n";
	stream << "end_header\n";

	// 1 line per vertex
	for (size_t i = 0; i < scatterEventVertices.size() / 3; i++)
	{
		stream << scatterEventVertices[3 * i] << " " << // x
			scatterEventVertices[3 * i + 1] << " " << // y
			scatterEventVertices[3 * i + 2] << " " << // z
			"\n";
	}

	// 2 additional vertices for reference/bounding box

	stream << exRes << " " << // x
		exRes << " " << // y
		exRes << " " << // z
		"\n";
	stream << 0.0f << " " << // x
		0.0f << " " << // y
		0.0f << " " << // z
		"\n";

	// 1 line per edge
	for (size_t i = 0; i < scatterEventEdges.size() / 2; i++)
	{
		stream << scatterEventEdges[2 * i] << " " <<
			scatterEventEdges[2 * i + 1] << " " <<
			"255 255 255" <<
			"\n";
	}

	stream.close();
}



void writeVolumeSHToDisk(const std::vector<std::vector<float>>& volumeSH, const char* fileName = fileNameVolumeSH)
{
	FILE* file;
	errno_t err;

	// open file for write
	err = fopen_s(&file, fileName, "wb");
	if (err == 0)
	{
		std::cout << "open to write successfull\n";
	}
	else
	{
		std::cout << "open to write failed\n";
	}

	std::cout << "writing data window SH content to disc...\n";

	// volumeSH.size() contains number of "rows"
	// volumeSH[i].size() contains number of "columns"
	for (size_t i = 0; i < volumeSH.size(); i++)
	{
		fwrite(&volumeSH[i][0], sizeof(float), volumeSH[i].size(), file);
	}

	fclose(file);
}


void loadVolumeSHFromDisk(std::vector<std::vector<float>>& volumeSH, const char* fileName = fileNameVolumeSH)
{
	FILE* file;
	errno_t err;

	//open to read data from file to array

	err = fopen_s(&file, fileName, "rb");
	if (err == 0)
	{
		std::cout << "open to read successful\n";
	}
	else
	{
		std::cout << "open to read failed\n";
		return;
	}

	std::cout << "reading data window SH content from disc...\n";

	for (size_t i = 0; i < volumeSH.size(); i++)
	{
		for (size_t j = 0; j < volumeSH[i].size(); j++)
		{
			float v;
			fread(&v, sizeof(float), 1, file);
			volumeSH[i][j] = v;
		}
	}

	fclose(file);
}



void writeOriginalRadianceToDisk(const std::vector<SHSample>& samples)
{
	std::cout << "writing original radiance to disk...\n";

	std::ofstream stream("original radiance.ply");

	// header
	stream << "ply\n";
	stream << "format ascii 1.0\n";
	stream << "element vertex " << samples.size() + 2 << "\n";
	stream << "property float x\n";
	stream << "property float y\n";
	stream << "property float z\n";
	/*stream << "property float nx\n";
	stream << "property float ny\n";
	stream << "property float nz\n";*/
	stream << "end_header\n";

	// 1 line per vertex
	for (size_t i = 0; i < samples.size(); i++)
	{
		stream << samples[i].unitVec[0] * samples[i].intensity << " " << // x
			samples[i].unitVec[1] * samples[i].intensity << " " << // y
			samples[i].unitVec[2] * samples[i].intensity << " " << // z
			//samples[i].unitVec[0] << " " << // nx
			//samples[i].unitVec[1] << " " << // ny
			//samples[i].unitVec[2] << " " << // nz
			"\n";
	}

	// 2 additional vertices for reference/bounding box
	stream << 1.0f << " " << // x
		1.0f << " " << // y
		1.0f << " " << // z
		//1.0f << " " << // nx
		//1.0f << " " << // ny
		//1.0f << " " << // nz
		"\n";

	stream << -1.0f << " " << // x
		-1.0f << " " << // y
		-1.0f << " " << // z
		//-1.0f << " " << // nx
		//-1.0f << " " << // ny
		//-1.0f << " " << // nz
		"\n";

	stream.close();
}


void writeSphericalHarmonicsToDisk(const std::vector<SHSample>& samples)
{
	std::cout << "writing spherical harmonics to disk...\n";

	std::ofstream stream("spherical harmonics.ply");

	// header
	stream << "ply\n";
	stream << "format ascii 1.0\n";
	stream << "element vertex " << samples.size() + 2 << "\n";
	stream << "property float x\n";
	stream << "property float y\n";
	stream << "property float z\n";
	/*stream << "property float nx\n";
	stream << "property float ny\n";
	stream << "property float nz\n";*/
	stream << "end_header\n";

	// 1 line per vertex
	for (size_t i = 0; i < samples.size(); i++)
	{
		stream << samples[i].unitVec[0] * samples[i].intensity << " " << // x
			samples[i].unitVec[1] * samples[i].intensity << " " << // y
			samples[i].unitVec[2] * samples[i].intensity << " " << // z
			//samples[i].unitVec[0] << " " << // nx
			//samples[i].unitVec[1] << " " << // ny
			//samples[i].unitVec[2] << " " << // nz
			"\n";
	}

	// 2 additional vertices for reference/bounding box
	stream << 1.0f << " " << // x
		1.0f << " " << // y
		1.0f << " " << // z
		//1.0f << " " << // nx
		//1.0f << " " << // ny
		//1.0f << " " << // nz
		"\n";

	stream << -1.0f << " " << // x
		-1.0f << " " << // y
		-1.0f << " " << // z
		//-1.0f << " " << // nx
		//-1.0f << " " << // ny
		//-1.0f << " " << // nz
		"\n";

	stream.close();
}


void writeVolumeRadianceToDisk(const std::vector<float>& dWRadianceVolume)
{
	std::cout << "writing radiance array content to disc...\n";
	writeContainerAsBinary(dWRadianceVolume, fileNameRadiance);
}

