#pragma once
#include "vmath.h"


#include <vector>



struct SHSample {
	vmath::vec3 sphCoord;
	vmath::vec3 unitVec;
	
	std::vector<float> coeffs;
	float intensity;
};