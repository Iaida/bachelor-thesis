#include "clouds.h"
#include "CellularAutomaton.h"



int main()
{
	// seed for random numbers
	srand((unsigned int)time(NULL));


	// cellular automaton
	executeCellularAutomaton();
	
	/*loadVolumeFromDisk(dWDensityVolume);
	addPadding();*/

	
	
	// volume extinction
	volumeExtinction();
	writeVolumeExtinctionToDisk(exExtinctionVolume);

	//loadVolumeExtinctionFromDisk(exExtinctionVolume);


	// single Voxel Radiance
	//sphericalHarmonicsAtVoxel(exRes / 2, (exRes * 50 / 100), exRes / 2, true);

	
	// Spherical Harmonics for whole volume
	calculateVolumeSH();
	writeVolumeSHToDisk(volumeSH);

	//loadVolumeSHFromDisk(volumeSH);
	
	HGToSH(true);

	volumeRayCasting();

	
	//// TESTS

	// visualize volume radiance
	/*volumeRadiance();
	mapVolumeRadiance();
	writeVolumeRadianceToDisk();*/

	// sh similarity in 3x3 cube
	/*calculateSHSimilarity();
	writeVolumeSHSimilarityToDisk(volumeSHSimilarity);*/

	//float HGinSHRotated[orderSH*orderSH];
	//DirectX::XMVECTOR rotAxis = { 1.0f, 0.0f, 0.0f };
	//float rotAngle = (float)90 * (float)M_PI / 180.0f;
	//DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationAxis(rotAxis, rotAngle);
	//DirectX::XMSHRotate(HGinSHRotated, orderSH, rotMatrix, HGinSH);
	//// 0.43 bei beide HGinSH, -0.0044 bei einem 180� gedreht, + 0.005 bei einem 90� gedreht
	//float dot = /*normalizationFactor **/ DirectX::XMSHDot(orderSH, HGinSH, HGinSH);

	/*float HGinSHMultiplied[orderSH*orderSH];
	float normalizationCoeffs[orderSH*orderSH];
	for (int i = 0; i < orderSH*orderSH; i++)
	{
		normalizationCoeffs[i] = getNormalizationWeights(i);
	}

	DirectX::XMSHMultiply6(HGinSHMultiplied, HGinSH, normalizationCoeffs);
	float dot = DirectX::XMSHDot(orderSH, HGinSHMultiplied, HGinSHMultiplied);*/


	// rotate 1 vector(yAxis) to match another(raydir)
	/*DirectX::XMVECTOR yAxis = { 0.0f, 1.0f, 0.0f };
	DirectX::XMVECTOR randomVector = { randomFloat(), randomFloat(), randomFloat() };
	DirectX::XMVECTOR rayDir = DirectX::XMVector3Normalize(randomVector);
	DirectX::XMVECTOR cross = DirectX::XMVector3Cross(rayDir, yAxis);
	float angleBetweenVectors = DirectX::XMVectorGetX(DirectX::XMVector3AngleBetweenVectors(rayDir, yAxis));
	DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationAxis(cross, angleBetweenVectors);
	yAxis = DirectX::XMVector3Transform(yAxis, rotMatrix);*/
	

	std::cout << "finished, press enter to close\n";
	// finished, wait for enter to close console
	std::cin.get();

	return 0;
}
