#pragma once
#include "vmath.h"

const float samplingDistance = 1.0f;

struct ray
{
	vmath::vec3 origin;
	vmath::vec3 direction;
	float distanceAlongRay;
	float extinction;
	float radiance;


	vmath::vec3 getPointAlongRay() const
	{ 
		return origin + distanceAlongRay * direction; 
	}


	vmath::vec3 getPreviousPointAlongRay() const
	{ 
		return origin + (distanceAlongRay - samplingDistance) * direction; 
	}
};