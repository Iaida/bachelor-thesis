// NOT FINISHED

struct densityOctreeNode
{
	// AABB
	vmath::vec3 min;
	vmath::vec3 max;
	// child nodes, fixed order for cube
	densityOctreeNode* children[8];
	// density
	float density;

	vmath::vec3 getSplittingPoint()
	{
		return (min + max) / 2.0f;
	}
	int getNumberOfContainedVoxels()
	{
		int counter = 0;
		for (int z = int(min[2] + 0.5f); z < int(max[2] + 0.5f); z++)
		{
			for (int y = int(min[1] + 0.5f); y < int(max[1] + 0.5f); y++)
			{
				for (int x = int(min[0] + 0.5f); x < int(max[0] + 0.5f); x++)
				{
					counter++;
				}
			}
		}
		return counter;
	}
	bool sameValueInContainedVoxels()
	{
		bool first = true;
		int previousIndex = 0;
		int currentIndex = 0;
		for (int z = int(min[2] + 0.5f); z < int(max[2] + 0.5f); z++)
		{
			for (int y = int(min[1] + 0.5f); y < int(max[1] + 0.5f); y++)
			{
				for (int x = int(min[0] + 0.5f); x < int(max[0] + 0.5f); x++)
				{
					if (first == true)
					{
						currentIndex = dWVolumeIndexFromCoordinates(x, y, z);
						previousIndex = currentIndex;
						first = false;
					}

					else
					{
						currentIndex = dWVolumeIndexFromCoordinates(x, y, z);
						if (dWDensityVolume[currentIndex] != dWDensityVolume[previousIndex])
						{
							// at least 2 not the same
							return false;
						}
						previousIndex = currentIndex;
					}
				}
			}
		}
		// all the same
		// initialize density value
		density = dWDensityVolume[currentIndex];
		return true;
	}

	float getDensityAtVoxel(int i, int j, int k)
	{
		if (children != NULL)
		{
			// is an inner node
			vmath::vec3 split = getSplittingPoint();
			if (split[0] < i && split[1] < j && split[2] < k)
			{
				// 000
				return children[0]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] > i && split[1] < j && split[2] < k)
			{
				// 100
				return children[1]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] < i && split[1] > j && split[2] < k)
			{
				// 010
				return children[2]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] > i && split[1] > j && split[2] < k)
			{
				// 110
				return children[3]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] < i && split[1] < j && split[2] > k)
			{
				// 001
				return children[4]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] > i && split[1] < j && split[2] > k)
			{
				// 101
				return children[5]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] < i && split[1] > j && split[2] > k)
			{
				// 011
				return children[6]->getDensityAtVoxel(i, j, k);
			}
			else if (split[0] > i && split[1] > j && split[2] > k)
			{
				// 111
				return children[7]->getDensityAtVoxel(i, j, k);
			}
			// error
			return -1.0f;
		}
		else
		{
			// is a leaf
			return density;
		}
	}


	// codingshuttle
	static densityOctreeNode* buildDensityOctree(vmath::vec3 boxMin, vmath::vec3 boxMax)
	{
		densityOctreeNode* currentNode = new densityOctreeNode;
		currentNode->min = boxMin;
		currentNode->max = boxMax;

		// also assigns density, if possible
		if (currentNode->sameValueInContainedVoxels() == true)
		{
			// stop splitting, also occurs if only 1 voxel left
			return NULL;
		}

		// split into 8 cubes
		std::vector<vmath::vec3> cubesMin(8);
		std::vector<vmath::vec3> cubesMax(8);
		vmath::vec3 center = currentNode->getSplittingPoint();


		// 000
		cubesMin[0] = boxMin;
		cubesMax[0] = center;
		// 100
		cubesMin[1] = { center[0], boxMin[1], boxMin[2] };
		cubesMax[1] = { boxMax[0], center[1], center[2] };
		// 010
		cubesMin[2] = { boxMin[0], center[1], boxMin[2] };
		cubesMax[2] = { center[0], boxMax[1], center[2] };
		// 110
		cubesMin[3] = { center[0], center[1], boxMin[2] };
		cubesMax[3] = { boxMax[0], boxMax[1], center[2] };
		// 001
		cubesMin[4] = { boxMin[0], boxMin[1], center[2] };
		cubesMax[4] = { center[0], center[1], boxMax[2] };
		// 101
		cubesMin[5] = { center[0], boxMin[1], center[2] };
		cubesMax[5] = { boxMax[0], center[1], boxMax[2] };
		// 011
		cubesMin[6] = { boxMin[0], center[1], center[2] };
		cubesMax[6] = { center[0], boxMax[1], boxMax[2] };
		// 111
		cubesMin[7] = { center[0], center[1], center[2] };
		cubesMax[7] = { boxMax[0], boxMax[1], boxMax[2] };

		// loop through children for recursion

		for (int i = 0; i < 8; i++)
		{
			currentNode->children[i] = buildDensityOctree(cubesMin[i], cubesMax[i]);
		}
		return currentNode;
	}
};

densityOctreeNode *rootDensityOctree = NULL;
densityOctreeNode* buildDensityOctree(vmath::vec3, vmath::vec3);




void octreeTest()
{

	// NOT FINISHED

	// get density at specific voxel
	int i = 23;
	int j = 36;
	int k = 42;
	float testdensity1 = dWDensityVolume[dWVolumeIndexFromCoordinates(i, j, k)];

	// volume starts and ends between voxels
	rootDensityOctree = densityOctreeNode::buildDensityOctree({ -0.5f, -0.5f, -0.5f }, { (float)(dWRes - 1) + 0.5f, (float)(dWRes - 1) + 0.5f, (float)(dWRes - 1) + 0.5f });

	float testdensity2 = rootDensityOctree->getDensityAtVoxel(i, j, k);


};