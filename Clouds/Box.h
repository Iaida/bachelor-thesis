#pragma once
#include "vmath.h"

struct Box {
	vmath::vec3 min;
	vmath::vec3 max;

	bool PointInside(const vmath::vec3& p) const
	{
		return p[0] >= min[0] && p[0] <= max[0] &&
			p[1] >= min[1] && p[1] <= max[1] &&
			p[2] >= min[2] && p[2] <= max[2];
	}
};