#pragma once
#include "vmath.h"
#include "Color.h"
#include "Box.h"


// data window resolution
const int dWRes = 64;
//const int dWRes = 128;

// padding added to both ends to avoid boundary conditions
const int padding = 2;
// extents resolution for allocation
const int exRes = 2 * padding + dWRes;
// boundary for extents, used in for-loops
const int exBound = padding + dWRes;



// starting seeds for the cloud generation 
const int numSeeds = 20;
// number of simulation steps for the cloud simulation
const int simSteps = 13;
//const int simSteps = 30;
// simulation steps until cld becomes 0 after ext has become 1
const int extTime = 3;






//// weights for cld bool -> 0..1 float calculation with 3x3x3 cube
// weight of 6 direct neighbors which share a face of a cell
const float axisWeight = 1.0f / 27.0f;
// weight of 12 diagonal neighbors using Pythagoras
const float diagWeight = axisWeight * (1.0f - (sqrtf(2.0f) - 1.0f));
// weight of 8 corners of 3x3 cube using Pythagoras
const float cornerWeight = axisWeight * (1.0f - (sqrtf(3.0f) - 1.0f));
// rest is weight for own cell
const float selfWeight = 1.0f - (6.0f * axisWeight + 12.0f * diagWeight + 8.0f * cornerWeight);




//// path tracer variables
const int numDirections = 64;
const int numDirectionsSqrt = (int)sqrt(numDirections);
const int numSamplesPerDirection = 4;
const vmath::vec3 sunPos = { 0.0f, 10000.0f, 0.0f };



const unsigned int maxScatteringEvents = 5;



int dpi = 72;

int width = 1280;
int height = 720;
//int width = 1920;
//int height = 1080;

float aspectratio = (float)width / (float)height;



// -1 <= g <= +1 ranges from backscattering through isotropic scattering(0) to forward scattering
const float g = 0.9f;



// Spherical Harmonics: also called bands - DirectX::XMSH: min=2, max=6
const int orderSH = 6;




const Box volumeBox = 
{ 
	vmath::vec3{ 0.0f, 0.0f, 0.0f },
	vmath::vec3{ dWRes - 1, dWRes - 1, dWRes - 1 } 
};


const Box dataWindowBox
{
	vmath::vec3(padding, padding, padding),
	vmath::vec3(dWRes + padding, dWRes + padding, dWRes + padding)
};

const Box extendedBox
{
	vmath::vec3(0.0f, 0.0f, 0.0f),
	vmath::vec3(exRes, exRes, exRes)
};