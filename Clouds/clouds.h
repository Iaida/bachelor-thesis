#pragma once

#define _USE_MATH_DEFINES
#include "vmath.h"
#include "DirectXSH.h"

#include "ray.h"
#include "SHSample.h"
#include "Box.h"
#include "utility.h"
#include "config.h"
#include "fileManagement.h"


#include <time.h>
#include <omp.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <time.h>

#include <math.h>



// Volume with float values between 0 and 1
std::vector<float> exDensityVolume(exRes* exRes* exRes);

// Volume with float values between 0 and 1 without padding, contains result
std::vector<float> dWDensityVolume(dWRes* dWRes* dWRes);


// 1 float per voxel to indicate overall radiance at the voxelcenter
std::vector<float> dWRadianceVolume(dWRes* dWRes* dWRes);
// 1 float per voxel to indicate extinction-coefficient for a direct ray towards the light source
std::vector<float> exExtinctionVolume(exRes* exRes* exRes);


// vertices - 3 floats
std::vector<float> scatterEventVertices;
// edges - 2 ints
std::vector<int> scatterEventEdges;


std::vector<std::vector<float>> volumeSH(dWRes* dWRes* dWRes, std::vector<float>(orderSH* orderSH));

std::vector<float> HGinSH(orderSH* orderSH);


std::vector<float> volumeSHSimilarity(dWRes* dWRes* dWRes);



unsigned exVolumeIndexFromCoordinates(const unsigned i, const unsigned j, const unsigned k)
{
	return Index1DFrom3D(i, j, k, exRes);
}

unsigned dWVolumeIndexFromCoordinates(const unsigned i, const unsigned j, const unsigned k)
{
	return Index1DFrom3D(i, j, k, dWRes);
}






void removePadding()
{
	std::cout << "converting extended float array to data window array...\n";
	int dWIndex = 0;
	for (int z = padding; z < exBound; z++)
	{
		for (int y = padding; y < exBound; y++)
		{
			for (int x = padding; x < exBound; x++)
			{
				int exIndex = exVolumeIndexFromCoordinates(x, y, z);

				if (exDensityVolume[exIndex] != 0.0f)
				{
					dWDensityVolume[dWIndex] = exDensityVolume[exIndex];
				}
				dWIndex++;
			}
		}
	}
}

void addPadding()
{
	std::cout << "converting data window array to extended float array...\n";
	int dWIndex = 0;
	for (int z = padding; z < exBound; z++)
	{
		for (int y = padding; y < exBound; y++)
		{
			for (int x = padding; x < exBound; x++)
			{
				int exIndex = exVolumeIndexFromCoordinates(x, y, z);

				if (dWDensityVolume[dWIndex] != 0.0f)
				{
					exDensityVolume[exIndex] = dWDensityVolume[dWIndex];
				}
				dWIndex++;
			}
		}
	}
}





//
//vmath::vec3 getSurfaceNormal(vmath::vec3 point)
//{
//	// get 3 partial derivatives for the gradient
//
//	float x0 = floor(point[0]);
//	float x1 = ceil(point[0]);
//	float pdx = trilinearInterpolation(x1, point[1], point[2]) - 
//				trilinearInterpolation(x0, point[1], point[2]);
//
//	float y0 = floor(point[1]);
//	float y1 = ceil(point[1]);
//	float pdy = trilinearInterpolation(point[0], y1, point[2]) -
//		trilinearInterpolation(point[0], y0, point[2]);
//
//	float z0 = floor(point[2]);
//	float z1 = ceil(point[2]);
//	float pdz = trilinearInterpolation(point[0], point[1], z1) -
//		trilinearInterpolation(point[0], point[1], z0);
//	
//	vmath::vec3 gradient = { pdx, pdy, pdz };
//
//	return gradient;
//}








float scatteringRay(const vmath::vec3& voxelCenter, const vmath::vec3& pointOnUnitsphere)
{
	// count scattering events
	unsigned timesScattered = 0;

	ray scaRay = {
		voxelCenter, // origin
		vmath::normalize(pointOnUnitsphere - voxelCenter), // direction
		samplingDistance, // distance along ray, start at 1 step
		1.0f // extinction coefficient starts at max value
	};

	/*scatterEventVertices.push_back(voxelCenter[0]);
	scatterEventVertices.push_back(voxelCenter[1]);
	scatterEventVertices.push_back(voxelCenter[2]);*/


	// start random walk
	vmath::vec3 currentPoint;
	do
	{
		currentPoint = scaRay.getPointAlongRay();
		const float currentDensity = trilinearInterpolation(exDensityVolume, currentPoint[0], currentPoint[1], currentPoint[2], exVolumeIndexFromCoordinates);

		const vmath::vec3 previousPoint = scaRay.getPreviousPointAlongRay();
		const float previousDensity = trilinearInterpolation(exDensityVolume, previousPoint[0], previousPoint[1], previousPoint[2], exVolumeIndexFromCoordinates);

		const float averageDensity = (previousDensity + currentDensity) / 2.0f;

		// loses energy by moving through the medium
		scaRay.extinction *= getTransmittance(averageDensity);

		// random number 0 to 1
		const float rng = (float)rand() / (float)RAND_MAX;

		if (rng <= currentDensity)
		{
			// scatter event occurred at current point
			timesScattered++;


			//// save scattering event in array
			//scatterEventVertices.push_back(currentPoint[0]);
			//scatterEventVertices.push_back(currentPoint[1]);
			//scatterEventVertices.push_back(currentPoint[2]);

			//// save edge in array
			//int scatterEventIndex = (scatterEventVertices.size()/3) - 1;
			//scatterEventEdges.push_back(scatterEventIndex);
			//scatterEventEdges.push_back(scatterEventIndex-1);



			// add direct radiance at scattering location
			// multiply both extinction coefficients and weigh them with the angle between the old ray and the direction towards the light source
			const vmath::vec3 towardsLight = vmath::normalize(sunPos - currentPoint);

			const float angleBetweenVectors = acos(clip(vmath::dot(scaRay.direction, towardsLight), -1.0f, 1.0f));



			// fetch direct extinction from precomputation
			const float directExtinction = trilinearInterpolation(exExtinctionVolume, currentPoint[0], currentPoint[1], currentPoint[2], exVolumeIndexFromCoordinates);
			//float directExtinction = directRay(currentPoint);
			scaRay.radiance += weightHG(angleBetweenVectors) * directExtinction * scaRay.extinction;


			// new scattered ray starts at current location
			scaRay.origin = currentPoint;
			// rotated by an angle(distributed by phase function) around a random axis which is perpendicular to the old ray direction
			const vmath::vec3 randomVector = { randomFloat(), randomFloat(), randomFloat() };
			const vmath::vec3 rotAxis = vmath::normalize(vmath::cross(randomVector, scaRay.direction));
			const float rotAngle = angleHG();
			scaRay.direction = vmath::normalize(rotateVectorAroundAxis(scaRay.direction, rotAngle, rotAxis));
			// make 1 step along ray
			scaRay.distanceAlongRay = samplingDistance;

			// weigh extinction with phase function for angle between old direction and new direction?
			//scaRay.extinction *= weightHG(rotAngle);

		}
		else
		{
			// no scattering, go further along the ray
			scaRay.distanceAlongRay += samplingDistance;
		}

	} while (dataWindowBox.PointInside(currentPoint) && timesScattered < maxScatteringEvents);


	if (timesScattered >= maxScatteringEvents)
	{
		// ray reached max number of scattering events
		return scaRay.radiance;
	}

	// ray arrived at padding 

	// direct ray towards light source without scattering
	const vmath::vec3 towardsLight = vmath::normalize(sunPos - currentPoint);
	const float angleBetweenVectors = acos(clip(vmath::dot(scaRay.direction, towardsLight), -1.0f, 1.0f));
	const float directExtinction = trilinearInterpolation(exExtinctionVolume, currentPoint[0], currentPoint[1], currentPoint[2], exVolumeIndexFromCoordinates);
	//float directExtinction = directRay(currentPoint);
	scaRay.radiance += scaRay.extinction * directExtinction * weightHG(angleBetweenVectors);


	// add ambient term - result of mie scattering
	const float skyIntensity = (skyblue[0] + skyblue[1] + skyblue[2]) / 3.0f;
	scaRay.radiance += scaRay.extinction * skyIntensity;

	return scaRay.radiance;

}

void mapVolumeRadiance()
{
	std::cout << "mapping Volume Radiance to values between 0 and 1... \n";

	float max = *std::max_element(dWRadianceVolume.cbegin(), dWRadianceVolume.cend());

	for (float& radiance : dWRadianceVolume)
	{
		if (radiance > 0.0f)
		{
			radiance /= max;
		}
		else
		{
			// TODO: why not 0 ???
			radiance = 1.0f;
		}
	}
}



// returns just the extinction coefficient
float directRay(const vmath::vec3& loc)
{
	// spawn direct ray towards light source without scattering

	ray dirRay = {
		loc, // origin
		vmath::normalize(sunPos - loc), // direction
		samplingDistance, // distance along ray, start at 1 step
		1.0f, // extinction coefficient starts at max value
		0.0f // radiance not needed here
	};

	vmath::vec3 currentPoint = dirRay.getPointAlongRay();



	// might have to go through volume to reach light source
	if (!extendedBox.PointInside(currentPoint))
	{
		// went after first step outside of the volume
		// means the location is already at the side of the volume which already faces the light source	
		// doesn't occur ??? 
		return dirRay.extinction;
	}


	// went after first step back into volume, no scattering inside volume
	while (extendedBox.PointInside(currentPoint))
	{
		currentPoint = dirRay.getPointAlongRay();
		float currentDensity;
		// TODO: box
		if (currentPoint[0] > (float)exRes - 1.0f || currentPoint[1] > (float)exRes - 1.0f || currentPoint[2] > (float)exRes - 1.0f ||
			currentPoint[0] < 1.0f || currentPoint[1] < 1.0f || currentPoint[2] < 1.0f)
		{
			// bordercase
			currentDensity = 0.0f;
		}
		else
		{
			currentDensity = trilinearInterpolation(exDensityVolume, currentPoint[0], currentPoint[1], currentPoint[2], exVolumeIndexFromCoordinates);
		}

		const vmath::vec3 previousPoint = dirRay.getPreviousPointAlongRay();
		float previousDensity;
		if (previousPoint[0] > (float)exRes - 1.0f || previousPoint[1] > (float)exRes - 1.0f || previousPoint[2] > (float)exRes - 1.0f ||
			previousPoint[0] < 1.0f || previousPoint[1] < 1.0f || previousPoint[2] < 1.0f)
		{
			// bordercase
			previousDensity = 0.0f;
		}
		else
		{
			previousDensity = trilinearInterpolation(exDensityVolume, previousPoint[0], previousPoint[1], previousPoint[2], exVolumeIndexFromCoordinates);
		}

		const float averageDensity = (previousDensity + currentDensity) / 2.0f;

		// Transmittance is the inverse of the average density
		// reduce light quantity by a fraction during its path through the medium
		dirRay.extinction *= getTransmittance(averageDensity);

		//go further along the ray
		dirRay.distanceAlongRay += samplingDistance;
	}
	// reached outside as direct ray, looses no more energy
	return dirRay.extinction;
}

void volumeExtinction()
{
	// f�r jeden Voxel im dw -> extinction mit direktem ray in Richtung Sonne -> sp�ter trilin interp darauf -> exres^3 gr��e
	std::cout << "Calculating Volume Extinction...\n";

	int counter = 1;
	int percentDone = 0;

	int x, y, z, newPercentage, currentIndex;
#pragma omp parallel for private(y,x,newPercentage,currentIndex)
	for (z = 0; z < exRes; z++)
	{
		for (y = 0; y < exRes; y++)
		{
			for (x = 0; x < exRes; x++)
			{
				newPercentage = (int)(100 * (float)counter / (float)(exRes * exRes * exRes));
				if (newPercentage > percentDone + 4)
				{
					percentDone = newPercentage;
					std::cout << percentDone << "% done. Calculating voxel number " << counter << " of " << exRes * exRes * exRes << "...\n";
				}

				currentIndex = exVolumeIndexFromCoordinates(x, y, z);

				// spawn direct ray towards light source, which returns the extinction that occurs
				exExtinctionVolume[currentIndex] = directRay(vmath::vec3((float)x, (float)y, (float)z));
				counter++;
			}
		}
	}
}




// Gritty Details
std::vector<float> sphericalHarmonicsAtVoxel(int x, int y, int z, bool writeToDisk)
{
	if (writeToDisk)
	{
		std::cout << "calculating voxel radiance at " << x << ", " << y << ", " << z << "...\n";
	}

	// gets returned at the end
	std::vector<float> resultsSH(orderSH * orderSH);

	const vmath::vec3 voxelCenter = vmath::vec3(discreteToContinuous(x), discreteToContinuous(y), discreteToContinuous(z));

	// fill an N*N*2 array with uniformly distributed samples across the sphere using jittered stratification
	std::vector<SHSample> samplesSH(numDirections);

	int i = 0; // array index
	const float oneOverN = 1.0f / numDirectionsSqrt; // TODO: correct ???

	for (int a = 0; a < numDirectionsSqrt; a++)
	{
		for (int b = 0; b < numDirectionsSqrt; b++)
		{
			// generate unbiased distribution of spherical coords
			const float x = (a + canonicalRandomVariable()) * oneOverN;
			const float y = (b + canonicalRandomVariable()) * oneOverN;
			const float theta = 2.0f * acos(sqrt(1.0f - x));
			const float phi = 2.0f * (float)M_PI * y;
			samplesSH[i].sphCoord = { theta, phi, 1.0f };
			samplesSH[i].unitVec = { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };


			float accSamplesPerDirection = 0.0f;
			for (int s = 0; s < numSamplesPerDirection; s++)
			{
				accSamplesPerDirection += scatteringRay(voxelCenter, voxelCenter + samplesSH[i].unitVec);
			}
			// build average
			samplesSH[i].intensity = accSamplesPerDirection / (float)numSamplesPerDirection;
			//samplesSH[i].intensity = 1.0f;

			// Spherical Harmonics coefficients for this direction
			const DirectX::FXMVECTOR dirDX = { samplesSH[i].unitVec[0], samplesSH[i].unitVec[1], samplesSH[i].unitVec[2] };
			samplesSH[i].coeffs.resize(orderSH*orderSH);
			DirectX::XMSHEvalDirection(samplesSH[i].coeffs.data(), orderSH, dirDX);

			// iterate over coefficients
			for (int n = 0; n < orderSH * orderSH; n++)
			{
				resultsSH[n] += samplesSH[i].intensity * samplesSH[i].coeffs[n];
			}

			++i;// end, increase array index
		}
	}

	// divide the result by weight and number of samples, get rid of banding artifacts
	const float factor = 4.0f * (float)M_PI / numDirections;
	for (int n = 0; n < orderSH * orderSH; n++)
	{
		resultsSH[n] *= factor * getHeatKernel(n);
	}

	// DONE, final coefficients stored in local resultsSH

	if (writeToDisk)
	{
		writeOriginalRadianceToDisk(samplesSH);

		// reconstruct samples from spherical harmonics
		// iterate over all generated samples
		for (size_t d = 0; d < samplesSH.size(); d++)
		{
			float reconstructedIntensity = 0.0f;
			// iterate over coefficients
			for (int n = 0; n < orderSH * orderSH; n++)
			{
				reconstructedIntensity += resultsSH[n] * samplesSH[d].coeffs[n];
			}
			samplesSH[d].intensity = reconstructedIntensity;
		}

		writeSphericalHarmonicsToDisk(samplesSH);
	}

	return resultsSH;
}

void calculateVolumeSH()
{
	std::cout << "Calculating Spherical Harmonics for entire volume...\n";

	

	int counter = 1;
	int percentDone = 0;

	int x, y, z, newPercentage, currentIndex;
#pragma omp parallel for private(y,x,newPercentage, currentIndex)
	for (z = padding; z < exBound; z++)
	{
		for (y = padding; y < exBound; y++)
		{
			for (x = padding; x < exBound; x++)
			{
				newPercentage = (int)(100 * (float)counter / (float)(dWRes * dWRes * dWRes));
				if (newPercentage > percentDone)
				{
					percentDone = newPercentage;
					std::cout << percentDone << "% done. Calculating voxel number " << counter << " of " << dWRes * dWRes * dWRes << "... \n";
				}

				currentIndex = dWVolumeIndexFromCoordinates(x - padding, y - padding, z - padding);

				/*if (dWDensityVolume[currentIndex] != 0.0f )
				{
					volumeSH[currentIndex] = sphericalHarmonicsAtVoxel(x, y, z);
				}*/
				volumeSH[currentIndex] = sphericalHarmonicsAtVoxel(x, y, z, false);
				counter++;
			}
		}
	}
	//
	//	// 2nd pass
	//	std::cout << "2nd pass...\n";
	//	counter = 1;
	//	percentDone = 0;
	//#pragma omp parallel for private(y,x,newPercentage, currentIndex)
	//	for (z = padding; z < exBound; z++)
	//	{
	//		for (y = padding; y < exBound; y++)
	//		{
	//			for (x = padding; x < exBound; x++)
	//			{
	//				newPercentage = (int)(100 * (float)counter / (float)(dWRes*dWRes*dWRes));
	//				if (newPercentage > percentDone)
	//				{
	//					percentDone = newPercentage;
	//					std::cout << percentDone << "% done. Calculating voxel number " << counter << " of " << dWRes*dWRes*dWRes << "...\n";
	//				}
	//
	//				currentIndex = dWVolumeIndexFromCoordinates(x - padding, y - padding, z - padding);
	//
	//				if (hasNeighborWithDensity(x - padding, y - padding, z - padding))
	//				{
	//					volumeSH[currentIndex] = sphericalHarmonicsAtVoxel(x, y, z);
	//				}
	//				
	//				counter++;
	//			}
	//		}
	//	}

}



float getAccRadianceFromSH(const unsigned volumeIndex)
{
	float accDir = 0.0f;
	const float oneOverN = 1.0f / numDirectionsSqrt;

	for (int a = 0; a < numDirectionsSqrt; a++)
	{
		for (int b = 0; b < numDirectionsSqrt; b++)
		{
			// generate unbiased distribution of spherical coords
			const float x = (a + canonicalRandomVariable()) * oneOverN;
			const float y = (b + canonicalRandomVariable()) * oneOverN;
			const float theta = 2.0f * acos(sqrt(1.0f - x));
			const float phi = 2.0f * (float)M_PI * y;
			const vmath::vec3 sphCoord = { theta, phi, 1.0f };
			const vmath::vec3 unitVec = { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };

			// Spherical Harmonics coefficients for this direction
			std::vector<float> coeffs(orderSH * orderSH);
			const DirectX::FXMVECTOR dirDX = { unitVec[0], unitVec[1], unitVec[2] };
			DirectX::XMSHEvalDirection(&coeffs[0], orderSH, dirDX);

			float reconstructedIntensity = 0.0f;
			// iterate over coefficients
			for (int n = 0; n < orderSH * orderSH; n++)
			{
				reconstructedIntensity += volumeSH[volumeIndex][n] * coeffs[n];
			}

			accDir += reconstructedIntensity;
		}
	}// done with all directions
	return accDir;
}

void volumeRadiance()
{
	std::cout << "Calculating Volume Radiance...\n";

	int counter = 1;
	int percentDone = 0;

	int x, y, z, newPercentage, currentIndex;
#pragma omp parallel for private(y,x,newPercentage, currentIndex)
	for (z = padding; z < exBound; z++)
	{
		for (y = padding; y < exBound; y++)
		{
			for (x = padding; x < exBound; x++)
			{
				newPercentage = (int)(100 * (float)counter / (float)(dWRes * dWRes * dWRes));
				if (newPercentage > percentDone)
				{
					percentDone = newPercentage;
					std::cout << percentDone << "% done. Calculating voxel number " << counter << " of " << dWRes * dWRes * dWRes << "...\n";
				}

				currentIndex = dWVolumeIndexFromCoordinates(x - padding, y - padding, z - padding);


				if (dWDensityVolume[currentIndex] > 0.0f)
				{
					dWRadianceVolume[currentIndex] = getAccRadianceFromSH(currentIndex);
				}


				counter++;
			}
		}
	}
}






// TODO: performance, reuse 'trilinearInterpolation'
std::vector<float> trilinearInterpolateSH(float x, float y, float z)
{
	std::vector<float> resultSH(orderSH * orderSH);

	// iterate over coefficients
	for (int n = 0; n < orderSH * orderSH; n++)
	{

		int x0 = (int)floor(x);
		int x1 = (int)ceil(x);
		// x1 - x0 results in division by 0, if x whole-number
		//float xd = (x - x0) / (x1 - x0); 
		float xd = (x - x0);

		int y0 = (int)floor(y);
		int y1 = (int)ceil(y);
		//float yd = (y - y0) / (y1 - y0);
		float yd = (y - y0);

		int z0 = (int)floor(z);
		int z1 = (int)ceil(z);
		//float zd = (z - z0) / (z1 - z0);
		float zd = (z - z0);

		float c00 = volumeSH[dWVolumeIndexFromCoordinates(x0, y0, z0)][n] * (1 - xd) +
			volumeSH[dWVolumeIndexFromCoordinates(x1, y0, z0)][n] * xd;

		float c10 = volumeSH[dWVolumeIndexFromCoordinates(x0, y1, z0)][n] * (1 - xd) +
			volumeSH[dWVolumeIndexFromCoordinates(x1, y1, z0)][n] * xd;

		float c01 = volumeSH[dWVolumeIndexFromCoordinates(x0, y0, z1)][n] * (1 - xd) +
			volumeSH[dWVolumeIndexFromCoordinates(x1, y0, z1)][n] * xd;

		float c11 = volumeSH[dWVolumeIndexFromCoordinates(x0, y1, z1)][n] * (1 - xd) +
			volumeSH[dWVolumeIndexFromCoordinates(x1, y1, z1)][n] * xd;

		float c0 = c00 * (1 - yd) + c10 * yd;
		float c1 = c01 * (1 - yd) + c11 * yd;
		float c = c0 * (1 - zd) + c1 * zd;

		resultSH[n] = c;
	}

	return resultSH;
}


// TODO: performance
float dotProductSHRadiance(const ray& viewingRay)
{
	const vmath::vec3 currentPoint = viewingRay.getPointAlongRay();

	//float accIntensity = 0.0f;

	//vmath::vec3 towardsLight = vmath::normalize(sunPos - currentPoint);
	//float angleChangeTowardsLight = acos(clip(vmath::dot(viewingRay.direction, towardsLight), -1.0f, 1.0f));
	//float weightAngle = weightHG(angleChangeTowardsLight);
	//// fetch direct extinction from precomputation
	//float extinctionAtSample = trilinearInterpolation(exExtinctionVolume,
	//	currentPoint[0] + padding,
	//	currentPoint[1] + padding,
	//	currentPoint[2] + padding);

	//accIntensity += /*weightAngle **/ extinctionAtSample;


	// SH for current point from precomputation
	const std::vector<float> currentSH = trilinearInterpolateSH(currentPoint[0], currentPoint[1], currentPoint[2]);

	// build rotation matrix to rotate HGinSH(yAxis) to match the inverse ray direction
	const DirectX::XMVECTOR yAxis = { 0.0f, 1.0f, 0.0f };
	const DirectX::XMVECTOR invRayDirDX = { -viewingRay.direction[0], -viewingRay.direction[1], -viewingRay.direction[2] };
	const DirectX::XMVECTOR cross = DirectX::XMVector3Cross(invRayDirDX, yAxis);
	const float angleBetweenVectors = DirectX::XMVectorGetX(DirectX::XMVector3AngleBetweenVectors(invRayDirDX, yAxis));
	const DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationAxis(cross, angleBetweenVectors);
	std::vector<float>HGinSHRotated(orderSH * orderSH);

	DirectX::XMSHRotate(&HGinSHRotated[0], orderSH, rotMatrix, &HGinSH[0]);

	const float dot = DirectX::XMSHDot(orderSH, &currentSH[0], &HGinSHRotated[0]);

	return dot;

	//accIntensity += dot;

	//// single scattering, ray towards light source
	//viewingRay.direction = towardsLight;

	//// build rotation matrix to rotate HGinSH(yAxis) to match the inverse ray direction
	//DirectX::XMVECTOR invRayDirDXScatter = { -towardsLight[0], -towardsLight[1], -towardsLight[2] };
	//DirectX::XMVECTOR crossScatter = DirectX::XMVector3Cross(invRayDirDX, yAxis);
	//float angleBetweenVectorsScatter = DirectX::XMVectorGetX(DirectX::XMVector3AngleBetweenVectors(invRayDirDX, yAxis));
	//DirectX::XMMATRIX rotMatrixScatter = DirectX::XMMatrixRotationAxis(cross, angleBetweenVectors);
	//std::vector<float>HGinSHRotatedScatter(orderSH*orderSH);
	//DirectX::XMSHRotate(&HGinSHRotatedScatter[0], orderSH, rotMatrixScatter, &HGinSH[0]);

	//// start at 1 step
	//viewingRay.distanceAlongRay = samplingDistance;

	//while (pointInBox(viewingRay.getPointAlongRay()))
	//{
	//	// extinction from current point towards original scattering point
	//	float currentExtinction = trilinearInterpolation(exExtinctionVolume, 
	//		currentPoint[0] + padding, 
	//		currentPoint[1] + padding,
	//		currentPoint[2] + padding);

	//	float extinctionDiff = currentExtinction - extinctionAtSample;

	//	// SH for current point
	//	vmath::vec3 currentPointScatter = viewingRay.getPointAlongRay();
	//	std::vector<float> currentSHScatter = trilinearInterpolateSH(currentPoint[0], currentPoint[1], currentPoint[2]);

	//	float dotScatter = DirectX::XMSHDot(orderSH, &currentSHScatter[0], &HGinSHRotatedScatter[0]);
	//	
	//	accIntensity += extinctionDiff*(dotScatter + weightAngle * currentExtinction);

	//	viewingRay.distanceAlongRay += samplingDistance;
	//}


	//return accIntensity;
}

// returns pixel color and alpha
// TODO: performance
ColorF calcPixel(const ray& pixelRay, const Box& volumeBox)
{
	const auto minMaxOpt = rayBoxIntersection(pixelRay, volumeBox);


	if (!minMaxOpt)
	{
		// no intersection, opaque black
		return black;
	}

	// ray and box intersect

	// for intersection testing
	//return white;

	// initial values
	float alpha = 0.0f;
	ColorF color = black;

	// create front-to-back ray
	ray currentRay;
	currentRay.origin = pixelRay.origin + minMaxOpt.value()[0] * pixelRay.direction;
	currentRay.direction = pixelRay.direction;
	currentRay.distanceAlongRay = samplingDistance;

	// step along ray until it reaches front of the volume or enough opacity is accumulated
	while (volumeBox.PointInside(currentRay.getPointAlongRay()) && alpha < 0.95f)
	{
		// save old results
		const float alphaOld = alpha;
		const vmath::vec3 colorOld = color;

		const vmath::vec3 currentPoint = currentRay.getPointAlongRay();

		// current density -> alpha
		const float alphaCurrent = trilinearInterpolation(exDensityVolume,
			currentPoint[0] + (float)padding,
			currentPoint[1] + (float)padding,
			currentPoint[2] + (float)padding,
			exVolumeIndexFromCoordinates);

		// compositing
		alpha = alphaOld + alphaCurrent * (1.0f - alphaOld);

		float intensity = 0.0f;
		if (alphaCurrent > 0.0f)
		{
			//intensity = 1.0f - currentDensity;
			/*intensity = trilinearInterpolation(exExtinctionVolume,
				currentPoint[0] + (float)padding,
				currentPoint[1] + (float)padding,
				currentPoint[2] + (float)padding);*/

			intensity = dotProductSHRadiance(currentRay);
		}

		// current
		const vmath::vec3 colorCurrent = white * intensity;
		// compositing
		color = colorOld + colorCurrent * alphaCurrent * (1.0f - alphaOld);



		currentRay.distanceAlongRay += samplingDistance;
	}

	return color;
}




void toneMapping(std::vector<ColorF>& pixels)
{

	// global tone mapping - Reinhard: Photographic Tone Reproduction for Digital Images
	std::cout << "Tone Mapping... \n";

	// Calculate logarithmic average luminance
	float accLuminance = 0.0f;
	// to avoid singularities for black pixels
	const float delta = 0.01f;
	for (const ColorF& pixel : pixels)
	{
		accLuminance += log(delta + colorLuminance(pixel));
	}

	// original
	//float averageLuminance = (1.0f / (float)pixels.size()) * exp(accLuminance);
	// shifted
	//float averageLuminance = exp(log(1.0f / (float)pixels.size()) + accLuminance);
	// own
	const float averageLuminance = exp((1.0f / (float)pixels.size()) * accLuminance);

	// target average luminance
	const float a = 0.1f;

	for (size_t p = 0; p < pixels.size(); p++)
	{
		const float pixelLuminance = 0.299f * pixels[p][0] + 0.587f * pixels[p][1] + 0.144f * pixels[p][2];
		const float scaleLuminance = (a / averageLuminance) * pixelLuminance;
		// equation 3
		pixels[p][0] = scaleLuminance / (1.0f + scaleLuminance);
		pixels[p][1] = scaleLuminance / (1.0f + scaleLuminance);
		pixels[p][2] = scaleLuminance / (1.0f + scaleLuminance);
		// equation 4
		/*float whiteLuminance = 10.0f;
		pixels[p].color[0] = (scaleLuminance + ((scaleLuminance*scaleLuminance) / (whiteLuminance*whiteLuminance))) / (1.0f + scaleLuminance);
		pixels[p].color[1] = (scaleLuminance + ((scaleLuminance*scaleLuminance) / (whiteLuminance*whiteLuminance))) / (1.0f + scaleLuminance);
		pixels[p].color[2] = (scaleLuminance + ((scaleLuminance*scaleLuminance) / (whiteLuminance*whiteLuminance))) / (1.0f + scaleLuminance);*/
	}

	savebmp("Cloud +tonemapping.bmp", width, height, dpi, pixels);

	//// Apply background Color
	//std::cout << "Applying background color...\n";

	//for (size_t p = 0; p < pixels.size(); p++)
	//{
	//	pixels[p].color = pixels[p].color*pixels[p].alpha + white*(1.0f - pixels[p].alpha);
	//	/*pixels[p].color[0] = clip(pixels[p].color[0], 0.0f, 1.0f);
	//	pixels[p].color[1] = clip(pixels[p].color[1], 0.0f, 1.0f);
	//	pixels[p].color[2] = clip(pixels[p].color[2], 0.0f, 1.0f);*/
	//}
	//
	//savebmp("Cloud +tonemapping +white background.bmp", width, height, dpi, pixels);
}


void volumeRayCasting()
{
	std::cout << "Volume Ray Casting...\n";


	const clock_t start = clock();

	// setup Volume


	// setup camera

	// origin, diagonally below
	vmath::vec3 eyePos = { 0.0f, 0.0f, 0.0f };
	//vmath::vec3 eyePos = { 0.1f * (float)(dWRes - 1), 0.1f * (float)(dWRes - 1), 0.1f * (float)(dWRes - 1) };
	// slightly below from the side
	//vmath::vec3 eyePos = { 0.4f * (float)(dWRes - 1), 0.4f * (float)(dWRes - 1), -0.125f * (float)(dWRes - 1) };
	// looking down
	//vmath::vec3 eyePos = { 0.4f * (float)(dWRes - 1), 1.125f * (float)(dWRes - 1), 0.4f * (float)(dWRes - 1) };
	// looking up
	//vmath::vec3 eyePos = { 0.4f * (float)(dWRes - 1), -0.125f * (float)(dWRes - 1), 0.4f * (float)(dWRes - 1) };


	// middle of volume
	const vmath::vec3 lookAt = { 0.5f * (float)(dWRes - 1), 0.5f * (float)(dWRes - 1), 0.5f * (float)(dWRes - 1) };
	const vmath::vec3 worldUp = { 0.0f, 1.0f, 0.0f };
	const vmath::vec3 right = vmath::normalize(vmath::cross(vmath::normalize(eyePos - lookAt), worldUp));
	const vmath::vec3 up = vmath::normalize(vmath::cross(right, vmath::normalize(eyePos - lookAt)));

	const float fovDeg = 90.0f;
	const float fovRad = fovDeg * (float)M_PI / 180.0f;
	// distance eye to image plane
	const float d = 1.0f;

	// image plane normal towards eye
	const vmath::vec3 n = vmath::normalize(eyePos - lookAt);
	// image plane vector right
	const vmath::vec3 u = vmath::normalize(vmath::cross(up, n));
	// image plane vector up
	const vmath::vec3 v = vmath::cross(n, u);
	// image plane height
	const float H = tan(fovRad / 2.0f) * 2.0f * d;
	// image plane width 
	const float W = H * aspectratio;
	// center of image plane
	const vmath::vec3 C = eyePos - n * d;
	// bottow left corner of image plane
	const vmath::vec3 L = C - u * W / 2.0f - v * H / 2.0f;
	const float pixelWidth = W / width;
	const float pixelHeight = H / height;


	// create bitmap from bottom to top, left to right

	std::vector<ColorF> pixels(width * height);

	int counter = 1;
	int percentDone = 0;

	int x, y, newPercentage, currentIndex;
	vmath::vec3 s, rayDir;
	ray currentRay;
#pragma omp parallel for private(x,newPercentage, currentIndex, s, rayDir, currentRay)
	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++)
		{
			newPercentage = (int)(100 * (float)counter / (float)(height * width));
			if (newPercentage > percentDone)
			{
				percentDone = newPercentage;
				std::cout << percentDone << "% done. Calculating pixel number " << counter << " of " << height * width << "...\n";
			}

			currentIndex = y * width + x;

			// pixel location
			s = L + u * (float)x * pixelWidth + v * (float)y * pixelHeight;
			// pixel center
			s += vmath::vec3{ pixelWidth / 2.0f, pixelHeight / 2.0f, 0.0f };
			rayDir = vmath::normalize(s - eyePos);
			currentRay = { s, rayDir, 0.0f, 1.0f, 0.0f };

			pixels[currentIndex] = calcPixel(currentRay, volumeBox);

			counter++;
		}
	}



	savebmp("Cloud -tonemapping.bmp", width, height, dpi, pixels);

	toneMapping(pixels);


	const clock_t end = clock();
	float diff = ((float)end - (float)start) / 1000;
	std::cout << "Volume Ray Casting took " << diff << " seconds.\n";
}




float pathtracingRadiance(const ray& viewingRay)
{

	vmath::vec3 pos = viewingRay.getPointAlongRay();
	float accDirections = 0.0f;
	float oneOverN = 1.0f / numDirectionsSqrt;

	for (int a = 0; a < numDirectionsSqrt; a++)
	{
		for (int b = 0; b < numDirectionsSqrt; b++)
		{

			// generate unbiased distribution of spherical coords
			float x = (a + canonicalRandomVariable()) * oneOverN;
			float y = (b + canonicalRandomVariable()) * oneOverN;
			float theta = 2.0f * acos(sqrt(1.0f - x));
			float phi = 2.0f * (float)M_PI * y;

			vmath::vec3 unitVector = { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };


			float accSamplesPerDirection = 0.0f;
			for (int s = 0; s < numSamplesPerDirection; s++)
			{
				accSamplesPerDirection += scatteringRay(pos, pos + unitVector);
			}
			// build average
			accSamplesPerDirection = accSamplesPerDirection / (float)numSamplesPerDirection;

			// weigh it with HG for this direction
			float angleBetweenVectors = acos(clip(vmath::dot(viewingRay.direction, -unitVector), -1.0f, 1.0f));
			accSamplesPerDirection *= weightHG(angleBetweenVectors);

			accDirections += accSamplesPerDirection;
		}
	}
	// build average
	return accDirections / (float)numDirections;
}

float reconstructedFromSHRadiance(const ray& viewingRay)
{
	// reconstruct samples from spherical harmonics

	float accDirections = 0.0f;

	const float oneOverN = 1.0f / numDirectionsSqrt;

	for (int a = 0; a < numDirectionsSqrt; a++)
	{
		for (int b = 0; b < numDirectionsSqrt; b++)
		{
			// generate unbiased distribution of spherical coords
			const float x = (a + canonicalRandomVariable()) * oneOverN;
			const float y = (b + canonicalRandomVariable()) * oneOverN;
			const float theta = 2.0f * acos(sqrt(1.0f - x));
			const float phi = 2.0f * (float)M_PI * y;
			const vmath::vec3 sphCoord = { theta, phi, 1.0f };
			const vmath::vec3 unitVec = { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };


			// Spherical Harmonics coefficients for this direction
			std::vector<float> coeffs(orderSH * orderSH);
			const DirectX::FXMVECTOR dirDX = { unitVec[0], unitVec[1], unitVec[2] };
			DirectX::XMSHEvalDirection(&coeffs[0], orderSH, dirDX);

			// SH for current point
			const vmath::vec3 currentPoint = viewingRay.getPointAlongRay();
			const std::vector<float> currentSH = trilinearInterpolateSH(currentPoint[0], currentPoint[1], currentPoint[2]);

			float reconstructedIntensity = 0.0f;
			// iterate over coefficients
			for (int n = 0; n < orderSH * orderSH; n++)
			{
				reconstructedIntensity += currentSH[n] * coeffs[n];
			}

			// got intensity for this direction

			// weigh it with HG for this direction
			const float angleBetweenVectors = acos(clip(vmath::dot(viewingRay.direction, -unitVec), -1.0f, 1.0f));
			reconstructedIntensity *= weightHG(angleBetweenVectors);

			accDirections += reconstructedIntensity;
		}
	}
	// build average
	return accDirections / (float)numDirections;
}



// get a good SH Approximation of the Henrey-Greenstein function with modified sampling
void HGToSH(bool writeToDisk, int numSampledDirections = 4096)
{
	std::cout << "calculating HG in SH...\n";

	// gets saved at the end
	std::vector<float> resultsSH(orderSH * orderSH);

	// rotated upwards
	const vmath::vec3 yAxis = { 0.0f, 1.0f, 0.0f };

	// fill an array with samples
	std::vector<SHSample> samplesSH(numSampledDirections);

	for (SHSample& sample : samplesSH)
	{
		// up vector rotated by an angle(distributed by phase function) around a random axis which is perpendicular to the up vector
		const vmath::vec3 randomVector = { randomFloat(), randomFloat(), randomFloat() };
		const vmath::vec3 rotAxis = vmath::normalize(vmath::cross(randomVector, yAxis));
		const float rotAngle = angleHG();
		sample.unitVec = vmath::normalize(rotateVectorAroundAxis(yAxis, rotAngle, rotAxis));

		sample.intensity = weightHG(rotAngle);

		// Spherical Harmonics coefficients for this direction

		const DirectX::FXMVECTOR dirDX = { sample.unitVec[0], sample.unitVec[1], sample.unitVec[2] };
		// initialize vector in struct
		sample.coeffs.resize(orderSH*orderSH);
		DirectX::XMSHEvalDirection(sample.coeffs.data(), orderSH, dirDX);

		// iterate over coefficients
		for (int n = 0; n < orderSH * orderSH; n++)
		{
			resultsSH[n] += sample.intensity * sample.coeffs[n];
		}
	}
	   
	
	// divide the result by weight and number of samples, get rid of banding artifacts
	const float factor = 0.5f * static_cast<float>(M_PI) / numSampledDirections;
	for (int n = 0; n < orderSH * orderSH; n++)
	{
		resultsSH[n] *= factor * getHeatKernel(n);
	}

	// save result
	HGinSH = resultsSH;


	// DONE, final coefficients stored in local resultsSH

	if (writeToDisk)
	{
		writeOriginalRadianceToDisk(samplesSH);

		// reconstruct samples from spherical harmonics
		// iterate over all generated samples
		for (size_t d = 0; d < samplesSH.size(); d++)
		{
			float reconstructedIntensity = 0.0f;
			// iterate over coefficients
			for (int n = 0; n < orderSH * orderSH; n++)
			{
				reconstructedIntensity += resultsSH[n] * samplesSH[d].coeffs[n];
			}
			samplesSH[d].intensity = reconstructedIntensity;
		}

		writeSphericalHarmonicsToDisk(samplesSH);
	}

	
	
	

	/*std::vector<float> HGinSHNormalized = normalizeSH(HGinSH);
	float dotResult1 = DirectX::XMSHDot(orderSH, &HGinSH[0], &HGinSH[0]);
	float dotResult2 = clip(DirectX::XMSHDot(orderSH, &HGinSHNormalized[0], &HGinSHNormalized[0]), -1.0f, 1.0f);
	float dotResult3 = lengthSH(HGinSH) * lengthSH(HGinSH) * dotResult2;
	float test = dotResult3 * (1.0f / (lengthSH(HGinSH) * lengthSH(HGinSH)));*/


	/*
	std::vector<float> HGinSHNormalizedRotated(orderSH*orderSH);
	DirectX::XMVECTOR rotAxis = { 1.0f, 0.0f, 0.0f };
	float rotAngle = (float)180 * (float)M_PI / 180.0f;
	DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationAxis(rotAxis, rotAngle);
	DirectX::XMSHRotate(&HGinSHNormalizedRotated[0], orderSH, rotMatrix, &HGinSHNormalized[0]);
	float dotResult4 = DirectX::XMSHDot(orderSH, &HGinSHNormalizedRotated[0], &HGinSHNormalized[0]);*/

}







//
//bool hasNeighborWithDensity(int i, int j, int k)
//{
//	// 3x3 cube, data window, check for densities !=0
//	
//	if (// z-1 slice
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j + 1, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j - 1, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j + 1, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j - 1, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j + 1, k - 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j - 1, k - 1)] != 0.0f ||
//		// z slice
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j + 1, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j - 1, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j + 1, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j - 1, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j + 1, k)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j - 1, k)] != 0.0f ||
//		// z+1 slice
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j + 1, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i, j - 1, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j + 1, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i + 1, j - 1, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j + 1, k + 1)] != 0.0f ||
//		dWDensityVolume[dWVolumeIndexFromCoordinates(i - 1, j - 1, k + 1)] != 0.0f )
//	{
//		return true;
//	}
//	else
//	{
//		return true;
//	}
//}
