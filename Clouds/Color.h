#pragma once
#include "vmath.h"

#include <tuple>

using ColorF = vmath::vec3;



const ColorF white = { 1.0f, 1.0f, 1.0f };
const ColorF black = { 0.0f, 0.0f, 0.0f };
const ColorF skyblue = { 0.5294f, 0.8078f, 0.9215f };
const ColorF red = { 1.0f, 0.0f, 0.0f };
const ColorF green = { 0.0f, 1.0f, 0.0f };


struct ColorUChar
{
	unsigned char r, g, b;
};


ColorUChar ColorFloatToUChar(const ColorF & c)
{

	// float 0-1 to uchar 0-255
	const auto mapping = [](float f) -> unsigned char
	{
		return static_cast<unsigned char>(f * 255.0f);
	};

	return ColorUChar
	{
		mapping(c[0]),
		mapping(c[1]),
		mapping(c[2])
	};
}


float colorLuminance(const ColorF & c)
{
	return 0.299f * c[0] + 0.587f * c[1] + 0.144f * c[2];
}


