#pragma once

struct cellAttr
{
	bool act; // activation
	bool hum; // humidity/vapor
	bool cld; // cloud existance
	bool ext; // cloud extinction
	int extCountdown; // simulation steps left until cld becomes 0 after ext has become 1
};